package com.example.learning.model;

public class Video {
    private Integer id;
    private String name;
    private Integer typeid;
    private String type;
    private String description;
    private String path;

    public Video() {
    }

    public Video(Integer id, String name, Integer typeid, String type, String description, String path) {
        this.id = id;
        this.name = name;
        this.typeid = typeid;
        this.type = type;
        this.description = description;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
