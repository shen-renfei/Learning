package com.example.learning.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.learning.Application.MyApplication;
import com.example.learning.FligtPrincipleActivity;
import com.example.learning.Http.SpSituTopicFetch;
import com.example.learning.R;
import com.example.learning.model.QTypes;
import com.example.learning.model.SpsituTopic;


public class SpSituFragment extends Fragment implements View.OnClickListener{
    private Button sp_situ;
    private Button sp_search;
    private static final String Sp_Situation="4";
    int flag=0;
    private String[] items;
    public SpSituFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_spsitu, container, false);
        sp_situ=v.findViewById(R.id.sp_situation);
        sp_situ.setOnClickListener(this);
        sp_search=v.findViewById(R.id.sp_search);
        sp_search.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        int i=view.getId();
        Intent intent=new Intent(getContext(), FligtPrincipleActivity.class);
        switch (i){
            case R.id.sp_situation:
                new SpSituTopicSearchTask().execute();
                break;
            case R.id.sp_search:
                Toast.makeText(getContext(),"搜索功能完善中...",Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }
    private class SpSituTopicSearchTask extends AsyncTask<Void,Void,SpsituTopic[]> {

        @Override
        protected SpsituTopic[] doInBackground(Void... voids) {
            return new SpSituTopicFetch().fetchItems();
        }
        @Override
        protected void onPostExecute(SpsituTopic[] result) {
            if(result!=null){
                items= new String[result.length];
                for(int i=0;i<result.length;i++){
                    items[i]=result[i].getTopicname();
                }
                flag=0;
                AlertDialog dialog = new AlertDialog.Builder(getContext())
                        .setIcon(R.drawable.tip1)//设置标题的图片
                        .setTitle("选择类别：")//设置对话框的标题
                        .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                flag=which;
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MyApplication app = (MyApplication) getActivity().getApplication();
                                Intent intent=new Intent(getContext(), FligtPrincipleActivity.class);
                                intent.putExtra("type",Sp_Situation);
                                intent.putExtra("topicid",String.valueOf(result[flag].getTopicid()));
                                getActivity().startActivity(intent);
                                dialog.dismiss();

                            }
                        }).create();
                dialog.show();
            }else{
                Toast.makeText(getContext(),"nothing",Toast.LENGTH_SHORT).show();
            }
        }
    }
}