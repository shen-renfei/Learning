package com.example.learning.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.learning.FligtPrincipleActivity;
import com.example.learning.R;


public class ExpandFragment extends Fragment implements View.OnClickListener{
    private TextView back;
    private String[] items=new String[]{"飞行原理","机载设备","飞行操作"};
    private int flag=0;
    public Button flight_relate,flight_cns;
    private static final String Flight_Principle="1";
    private static final String Air_Equipment="2";
    private static final String Flight_Operation="3";
    private static final String Flight_Cns="5";
    public ExpandFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_expand, container, false);

        flight_relate=v.findViewById(R.id.flight_relate);
        flight_cns=v.findViewById(R.id.flight_cns);
        flight_relate.setOnClickListener(this);
        flight_cns.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        int i=view.getId();
        switch (i){
            case R.id.flight_cns:
                Intent intent=new Intent(getContext(), FligtPrincipleActivity.class);
                intent.putExtra("type",Flight_Cns);
                getActivity().startActivity(intent);
                break;
            case R.id.flight_relate:
                flag=0;
                AlertDialog dialog = new AlertDialog.Builder(getContext())
                        .setIcon(R.drawable.tip1)//设置标题的图片
                        .setTitle("选择主题：")//设置对话框的标题
                        .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                Toast.makeText(FligtPrincipleActivity.this, items[which]+"which:"+which, Toast.LENGTH_SHORT).show();
                                flag=which;
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent=new Intent(getContext(), FligtPrincipleActivity.class);
                                switch (flag){
                                    case 0:
                                        intent.putExtra("type",Flight_Principle);
                                        getActivity().startActivity(intent);
                                        break;
                                    case 1:
                                        intent.putExtra("type",Air_Equipment);
                                        getActivity().startActivity(intent);
                                        break;
                                    case 2:
                                        intent.putExtra("type",Flight_Operation);
                                        getActivity().startActivity(intent);
                                        break;
                                    default:
                                        break;
                                }
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
                break;
            default:
                break;
        }
    }
}