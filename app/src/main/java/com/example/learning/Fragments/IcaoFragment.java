package com.example.learning.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.learning.Application.MyApplication;
import com.example.learning.FligtPrincipleActivity;
import com.example.learning.Http.IcaoFetch;
import com.example.learning.Http.IcaoTopicFetch;
import com.example.learning.Http.SearchFetch;
import com.example.learning.IcaoActivity;
import com.example.learning.MainActivity;
import com.example.learning.R;
import com.example.learning.model.QTypes;
import com.example.learning.model.Topic;
import com.example.learning.model.Video;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Timer;
import java.util.TimerTask;


public class IcaoFragment extends Fragment implements View.OnClickListener {

    private Button start;

    private String[] items;
    int flag=0;
    boolean flag2=false;
    private String Typeid="0";
    private String Topicid="0";
    private IcaoSearchTask icaoSearchTask;
    public IcaoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_icao, container, false);
        start=(Button) v.findViewById(R.id.icao_start);
        start.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.icao_start:
                icaoSearchTask=new IcaoSearchTask();
                icaoSearchTask.execute();
                break;
            default:
                break;
        }
    }
    private class IcaoSearchTask extends AsyncTask<Void,Void, QTypes[]> {
        private Timer mTimer = new Timer();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // 开始计时
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // 超时后取消任务
                    if (getStatus() == Status.RUNNING) {
                        Log.d("jieshu","is running");
                        icaoSearchTask.cancel(true);
                    }
                }
            }, 3000); // 设置超时时间为3秒

        }

        @Override
        protected QTypes[] doInBackground(Void... voids) {
            return new IcaoFetch().fetchItems();
        }
        @Override
        protected void onPostExecute(QTypes[] result) {
            if(result!=null){
//                Toast.makeText(getContext(),result[0].getQuestion(),Toast.LENGTH_SHORT).show();
                items= new String[result.length];
                for(int i=0;i<result.length;i++){
                    items[i]=result[i].getName();
                }
                flag=0;
                AlertDialog dialog = new AlertDialog.Builder(getContext())
                        .setIcon(R.drawable.tip1)//设置标题的图片
                        .setTitle("选择类别：")//设置对话框的标题
                        .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                flag=which;
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MyApplication app = (MyApplication) getActivity().getApplication();
                                Log.d("MainActivity","Icao");
                                Typeid =String.valueOf(flag+1);
                                dialog.dismiss();
                                new IcaoTopicSearchTask().execute();
                            }
                        }).create();
                dialog.show();
            }
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
            // 取消计时
            mTimer.cancel();
            Toast.makeText(getContext(),"搜索超时,请检查网络或联系管理员",Toast.LENGTH_SHORT).show();
        }
    }

    private class IcaoTopicSearchTask extends AsyncTask<Void,Void, Topic[]>{

        @Override
        protected Topic[] doInBackground(Void... voids) {
            return new IcaoTopicFetch().fetchItems(Typeid);
        }


        @Override
        protected void onPostExecute(Topic[] result) {
            if(result!=null) {
                items= new String[result.length];
                for(int i=0;i<result.length;i++){
                    items[i]=result[i].getTopicname();
                }
                flag=0;
                AlertDialog dialog = new AlertDialog.Builder(getContext())
                        .setIcon(R.drawable.tip1)//设置标题的图片
                        .setTitle("选择主题：")//设置对话框的标题
                        .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                Toast.makeText(FligtPrincipleActivity.this, items[which]+"which:"+which, Toast.LENGTH_SHORT).show();
                                flag=which;
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                MyApplication app = (MyApplication)getApplication();
////                                Toast.makeText(FligtPrincipleActivity.this, "flag:"+flag, Toast.LENGTH_SHORT).show();
//                                String str=app.getAddress()+"/upload"+result[flag].getPath()+"/"+result[flag].getName();
//                                Toast.makeText(FligtPrincipleActivity.this, str, Toast.LENGTH_SHORT).show();
//                                jzvdStd.setUp(str
//                                        , result[flag].getDescription());
                                Topicid =String.valueOf(flag+1);
                                MyApplication app = (MyApplication) getActivity().getApplication();
//                                Toast.makeText(FligtPrincipleActivity.this, "flag:"+flag, Toast.LENGTH_SHORT).show();
//                                String str=app.getAddress()+"/upload"+result[flag].getPath()+"/"+result[flag].getName();
                                Intent intent=new Intent(getContext(), IcaoActivity.class);
                                intent.putExtra("typeid", Typeid);
                                intent.putExtra("topicid",Topicid);
//                                intent.putExtra("keywords",result[flag].getKeywords());
                                getActivity().startActivity(intent);
                                Log.d("MainActivity","Topic");
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
            }else{
                Toast.makeText(getContext(),"搜索结果不存在",Toast.LENGTH_SHORT).show();
            }
            }
        }


}