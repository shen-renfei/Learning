package com.example.learning.Http;

import android.net.Uri;

import com.example.learning.model.Topic;
import com.example.learning.model.Video;
import com.google.gson.Gson;

public class IcaoTopicFetch extends SingleHttpFetch{
    public Topic[] fetchItems(String typeid) {
        String url = Uri.parse(test+"/searchTopicByTypeId").buildUpon().appendQueryParameter("typeid",typeid).build().toString();
        String json = null;
        Topic[] stringArray;
        try {
            json = getUrl(url);
            Gson gson = new Gson();
            stringArray = gson.fromJson(json, Topic[].class);
        } catch (Exception e) {
            return null;
        }

        return stringArray;
    }
}
