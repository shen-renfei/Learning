package com.example.learning.Http;

import android.net.Uri;

import com.example.learning.model.Question;
import com.example.learning.model.SpsituVideo;
import com.google.gson.Gson;

public class SpSituVideoFetch extends SingleHttpFetch{
    public SpsituVideo[] fetchItems(String des,String topicid) {
        String url = Uri.parse(test+"/searchSpSituVideos").buildUpon().appendQueryParameter("description", des).appendQueryParameter("topicid",topicid).build().toString();
        String json = null;
        SpsituVideo[] stringArray;
        try {
            json = getUrl(url);
            Gson gson = new Gson();
            stringArray = gson.fromJson(json, SpsituVideo[].class);
        } catch (Exception e) {
            return null;
        }

        return stringArray;
    }
}
