package com.example.learning.Http;

import android.net.Uri;

import com.example.learning.model.QTypes;

import com.google.gson.Gson;

public class IcaoFetch extends SingleHttpFetch{

    public QTypes[] fetchItems() {
        String url = Uri.parse(test+"/searchQTypes").buildUpon().build().toString();
        String json = null;
        QTypes[] stringArray;
        try {
            json = getUrl(url);
            Gson gson = new Gson();
            stringArray = gson.fromJson(json, QTypes[].class);
        } catch (Exception e) {
            return null;
        }

        return stringArray;
    }
}
