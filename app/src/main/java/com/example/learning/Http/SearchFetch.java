package com.example.learning.Http;

import android.net.Uri;
import android.util.Log;

import com.example.learning.model.Video;
import com.google.gson.Gson;

import java.io.IOException;

public class SearchFetch extends SingleHttpFetch{

    public Video[] fetchItems(String des,String typeid) {
        String url = Uri.parse(test+"/searchByDesAndTypeId").buildUpon().appendQueryParameter("description", des).appendQueryParameter("typeid",typeid).build().toString();
        String json = null;
        Video[] stringArray;
        try {
            json = getUrl(url);
            Gson gson = new Gson();
            stringArray = gson.fromJson(json, Video[].class);
        } catch (Exception e) {
            return null;
        }

        return stringArray;
    }
}
