package com.example.learning.Http;

import android.net.Uri;

import com.example.learning.model.SpsituTopic;
import com.google.gson.Gson;

public class SpSituTopicFetch extends SingleHttpFetch{

    public SpsituTopic[] fetchItems() {
        String url = Uri.parse(test+"/searchSpSituTopic").buildUpon().build().toString();
        String json = null;
        SpsituTopic[] stringArray;
        try {
            json = getUrl(url);
            Gson gson = new Gson();
            stringArray = gson.fromJson(json, SpsituTopic[].class);
        } catch (Exception e) {
            return null;
        }

        return stringArray;
    }
}
