package com.example.learning.Http;

import android.net.Uri;

import com.example.learning.model.QTypes;
import com.example.learning.model.Question;
import com.google.gson.Gson;

public class IcaoQuestionFetch extends SingleHttpFetch{


    public Question[] fetchItems(String typeid,String topicid) {
        String url = Uri.parse(test+"/searchQuestions").buildUpon().appendQueryParameter("typeid",typeid).appendQueryParameter("topicid",topicid).build().toString();
        String json = null;
        Question[] stringArray;
        try {
            json = getUrl(url);
            Gson gson = new Gson();
            stringArray = gson.fromJson(json, Question[].class);
        } catch (Exception e) {
            return null;
        }

        return stringArray;
    }
}
