package com.example.learning;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.HashMap;
import java.util.Map;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public Button homepage;

    private String videoUrl = "https://v-cdn.zjol.com.cn/280443.mp4";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        homepage=findViewById(R.id.homepage);
        homepage.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        int i=view.getId();
            if(i==R.id.homepage){
            Intent intent=new Intent(MainActivity.this,HomepageActivity.class);
            startActivity(intent);
        }
    }
}
