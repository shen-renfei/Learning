package com.example.learning.Application;

import android.app.Application;

public class MyApplication extends Application {
    private String address;
    private String home="http://192.168.31.142:8081";
    private String shiyanshi="http://10.12.188.184:8081";
    //test private
    private static MyApplication instance;
    public void onCreate(){
        super.onCreate();
//        setAddress(shiyanshi);
        setAddress(home);
        instance = this;
    }
    public static MyApplication getInstance() {
        return instance;
    }
    public void setAddress(String address) {
        this.address=address;
    }
    public String getAddress(){
        return address;
    }
}
