package com.example.learning;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.learning.Fragments.SpSituFragment;
import com.example.learning.Fragments.ExpandFragment;
import com.example.learning.Fragments.IcaoFragment;

import java.util.ArrayList;
import java.util.List;

public class HomepageActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager viewpager;
    private List<Fragment> list=new ArrayList<>();
    private FragmentAdapter adapter;
    private TextView icao,english,expand;
    private LinearLayout lin_learning,lin_english,lin_expand;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        viewpager=findViewById(R.id.viewpager);
        icao=findViewById(R.id.Icao_learning);
        english=findViewById(R.id.English_learning);
        expand=findViewById(R.id.Expand_learning);
        lin_learning=findViewById(R.id.Icao_learning_linear);
        lin_english=findViewById(R.id.Icao_english_linear);
        lin_expand=findViewById(R.id.Icao_expand_linear);
        list.add(new IcaoFragment());
        list.add(new SpSituFragment());
        list.add(new ExpandFragment());
        icao.setOnClickListener(this);
        english.setOnClickListener(this);
        expand.setOnClickListener(this);
        adapter=new FragmentAdapter(getSupportFragmentManager(),list);
        viewpager.setAdapter(adapter);
        viewpager.setCurrentItem(0);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        viewpager.setCurrentItem(0);
                        lin_learning.setBackgroundColor(Color.parseColor("#0B1428"));
                        lin_english.setBackground(null);
                        lin_expand.setBackground(null);
                        break;
                    case 1:
                        viewpager.setCurrentItem(1);
                        lin_learning.setBackground(null);
                        lin_english.setBackgroundColor(Color.parseColor("#0B1428"));
                        lin_expand.setBackground(null);
                        break;
                    case 2:
                        viewpager.setCurrentItem(2);
                        lin_learning.setBackground(null);
                        lin_english.setBackground(null);
                        lin_expand.setBackgroundColor(Color.parseColor("#0B1428"));
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        int i=view.getId();
        switch (i){
            case R.id.Icao_learning:
                viewpager.setCurrentItem(0);
                lin_learning.setBackgroundColor(Color.parseColor("#0B1428"));
                lin_english.setBackground(null);
                lin_expand.setBackground(null);
                break;
            case R.id.English_learning:
                viewpager.setCurrentItem(1);
                lin_learning.setBackground(null);
                lin_english.setBackgroundColor(Color.parseColor("#0B1428"));
                lin_expand.setBackground(null);
                break;
            case R.id.Expand_learning:
                viewpager.setCurrentItem(2);
                lin_learning.setBackground(null);
                lin_english.setBackground(null);
                lin_expand.setBackgroundColor(Color.parseColor("#0B1428"));
                break;
            default:
                break;
        }
    }
}