package com.example.learning;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.learning.Application.MyApplication;
import com.example.learning.Http.SearchFetch;
import com.example.learning.Http.SpSituVideoFetch;
import com.example.learning.model.SpsituVideo;
import com.example.learning.model.Video;

import java.util.Timer;
import java.util.TimerTask;

import cn.jzvd.JZDataSource;
import cn.jzvd.Jzvd;

public class FligtPrincipleActivity extends AppCompatActivity implements View.OnClickListener {
    private Button search;
    private EditText editText;
    private MyJzvdStd jzvdStd;
    private String typeId;
    private String SpsituTopicid;
    private SearchTask searchTask;
    private SpSituSearchTask spSituSearchTask;
    private static final String Sp_Situation="4";
    private boolean isSpsitu=false;
    int flag=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fligt_principle);
        Intent i=getIntent();
        typeId=i.getStringExtra("type");
        if(typeId.equals(Sp_Situation)){
            isSpsitu=true;
            SpsituTopicid=i.getStringExtra("topicid");
        }
        Log.d("FlightPrinciple",typeId);
        search=(Button) findViewById(R.id.Flight_Principle_button);
        editText=(EditText)findViewById(R.id.Flight_Principle_edit);
        search.setOnClickListener(this);
        jzvdStd = (MyJzvdStd) findViewById(R.id.Flight_Principle_video);
        MyApplication app = (MyApplication)getApplication();
//        Log.d("principle", "http://"+app.getAddress()+"/principle/video2.mp4");
//        jzvdStd.setUp(app.getAddress()+"/upload/video2.mp4"
//                , "Start Video");
        defaultVideoLoad(isSpsitu);
    }
    private void defaultVideoLoad(boolean flag){
        if(!flag){
            new SearchLoadTask().execute();
        }else{
            new SpSituSearchTask().execute();
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
    }

    @Override
    public void onBackPressed() {
        if (Jzvd.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        int i=view.getId();
        switch (i){
            case R.id.Flight_Principle_button:
                if(!isSpsitu){
                    searchTask=new SearchTask();
                    searchTask.execute();
                }else{
                    spSituSearchTask=new SpSituSearchTask();
                    spSituSearchTask.execute();
                }

                break;
            default:
                break;
        }
    }
    private class SearchLoadTask extends AsyncTask<Void,Void, Video[]> {
        private Timer mTimer = new Timer();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // 开始计时
//            Toast.makeText(FligtPrincipleActivity.this,"开始搜索",Toast.LENGTH_SHORT).show();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // 超时后取消任务
                    if (getStatus() == Status.RUNNING) {
                        Log.d("jieshu", "is running");
                        searchTask.cancel(true);
                    }
                }
            }, 3000); // 设置超时时间为3秒

        }

        @Override
        protected Video[] doInBackground(Void... voids) {
            return new SearchFetch().fetchItems(editText.getText().toString(), typeId);
        }

        @Override
        protected void onPostExecute(Video[] result) {
            if (result != null) {
                if (result.length == 0) {
                    Toast.makeText(FligtPrincipleActivity.this, "视频资源目录为空", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    MyApplication app = (MyApplication) getApplication();
                    String str = app.getAddress() + "/upload" + result[0].getPath() + "/" + result[0].getName();
                    jzvdStd.setUp(str, result[0].getDescription());
                }
            } else {
                Toast.makeText(FligtPrincipleActivity.this, "网络错误，默认视频资源加载失败", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private class SpSituSearchLoadTask extends AsyncTask<Void,Void, SpsituVideo[]> {
        private Timer mTimer = new Timer();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // 开始计时
//            Toast.makeText(FligtPrincipleActivity.this,"开始搜索",Toast.LENGTH_SHORT).show();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // 超时后取消任务
                    if (getStatus() == Status.RUNNING) {
                        Log.d("jieshu", "is running");
                        searchTask.cancel(true);
                    }
                }
            }, 3000); // 设置超时时间为3秒

        }

        @Override
        protected SpsituVideo[] doInBackground(Void... voids) {
            return new SpSituVideoFetch().fetchItems(editText.getText().toString(),SpsituTopicid);
        }

        @Override
        protected void onPostExecute(SpsituVideo[] result) {
            if (result != null) {
                if (result.length == 0) {
                    Toast.makeText(FligtPrincipleActivity.this, "视频资源目录为空", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    MyApplication app = (MyApplication) getApplication();
                    String str = app.getAddress() + "/upload" + result[0].getPath() + "/" + result[0].getName();
                    jzvdStd.setUp(str, result[0].getDescription());
                }
            } else {
                Toast.makeText(FligtPrincipleActivity.this, "网络错误，默认视频资源加载失败", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class SearchTask extends AsyncTask<Void,Void, Video[]> {
        private Timer mTimer = new Timer();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // 开始计时
//            Toast.makeText(FligtPrincipleActivity.this,"开始搜索",Toast.LENGTH_SHORT).show();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // 超时后取消任务
                    if (getStatus() == Status.RUNNING) {
                        Log.d("jieshu","is running");
                        searchTask.cancel(true);
                    }
                }
            }, 3000); // 设置超时时间为3秒

        }

        @Override
        protected Video[] doInBackground(Void... voids) {
            return new SearchFetch().fetchItems(editText.getText().toString(),typeId);
        }
        @Override
        protected void onPostExecute(Video[] result){
            if(result!=null){
                if(result.length==0){
                    Toast.makeText(FligtPrincipleActivity.this,"搜索结果不存在",Toast.LENGTH_SHORT).show();
                    return;
                }
                final String items[] = new String[result.length];
                for(int i=0;i<result.length;i++){
                    items[i]=result[i].getDescription();
                }
                flag=0;
                AlertDialog dialog = new AlertDialog.Builder(FligtPrincipleActivity.this)
                        .setIcon(R.drawable.tip1)//设置标题的图片
                        .setTitle("搜索结果：")//设置对话框的标题
                        .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                flag=which;
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MyApplication app = (MyApplication)getApplication();
                                String str=app.getAddress()+"/upload"+result[flag].getPath()+"/"+result[flag].getName();
                                jzvdStd.setUp(str
                                        , result[flag].getDescription());
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
            }else{
                Toast.makeText(FligtPrincipleActivity.this,"搜索结果不存在",Toast.LENGTH_SHORT).show();
            }

        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
            // 取消计时
            mTimer.cancel();
            Toast.makeText(FligtPrincipleActivity.this,"搜索超时,请检查网络或联系管理员",Toast.LENGTH_SHORT).show();
        }
    }
    private class SpSituSearchTask extends AsyncTask<Void,Void, SpsituVideo[]> {
        private Timer mTimer = new Timer();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // 开始计时
//            Toast.makeText(FligtPrincipleActivity.this,"开始搜索",Toast.LENGTH_SHORT).show();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // 超时后取消任务
                    if (getStatus() == Status.RUNNING) {
                        Log.d("jieshu","is running");
                        spSituSearchTask.cancel(true);
                    }
                }
            }, 3000); // 设置超时时间为3秒

        }
        @Override
        protected SpsituVideo[] doInBackground(Void... voids) {
            return new SpSituVideoFetch().fetchItems(editText.getText().toString(),SpsituTopicid);
        }
        @Override
        protected void onPostExecute(SpsituVideo[] result){
            if(result!=null) {
                if (result.length == 0) {
                    Toast.makeText(FligtPrincipleActivity.this, "搜索结果不存在", Toast.LENGTH_SHORT).show();
                    return;
                }
                final String items[] = new String[result.length];
                for(int i=0;i<result.length;i++){
                    items[i]=result[i].getDescription();
                }
                flag=0;
                AlertDialog dialog = new AlertDialog.Builder(FligtPrincipleActivity.this)
                        .setIcon(R.drawable.tip1)//设置标题的图片
                        .setTitle("搜索结果：")//设置对话框的标题
                        .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                flag=which;
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MyApplication app = (MyApplication)getApplication();
                                String str=app.getAddress()+"/upload"+result[flag].getPath()+"/"+result[flag].getName();
                                jzvdStd.setUp(str
                                        , result[flag].getDescription());
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
            }
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
            // 取消计时
            mTimer.cancel();
            Toast.makeText(FligtPrincipleActivity.this,"搜索超时,请检查网络或联系管理员",Toast.LENGTH_SHORT).show();
        }
    }
}