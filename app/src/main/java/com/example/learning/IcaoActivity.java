package com.example.learning;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.learning.Application.MyApplication;
import com.example.learning.Http.IcaoFetch;
import com.example.learning.Http.IcaoQuestionFetch;
import com.example.learning.Http.SearchFetch;
import com.example.learning.model.QTypes;
import com.example.learning.model.Question;

import java.io.IOException;
import java.util.Locale;

public class IcaoActivity extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener,MediaPlayer.OnCompletionListener,MediaPlayer.OnErrorListener{
    private Button play,prev,next,showanswer;
    private MediaPlayer player;
    private SeekBar mSeekBar;
    private TextView tv, tv2,keywords;
    private boolean hadDestroy = false;
    private boolean haDPlay = false;
    private CountDownTimer mCountDownTimer;
    private TextView mTimerText;
    private EditText mEditText;
    private boolean haDTimer=false;
    private boolean TimerComplete=false;
    private String answerText;
    private String typeid,topicid;
    private Question[] questions;
    private int videoNum=0;
    private final Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {

            switch (msg.what) {
                case 0x01:

                    break;

                default:
                    break;
            }
        };
    };


    Runnable runnable = new Runnable() {

        @Override
        public void run() {

            if (!hadDestroy) {

                mHandler.postDelayed(this, 100);
                int currentTime = Math
                        .round(player.getCurrentPosition() / 1000);
                String currentStr = String.format("%s%02d:%02d", "当前时间 ",
                        currentTime / 60, currentTime % 60);
                tv.setText(currentStr);
                mSeekBar.setProgress(player.getCurrentPosition());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icao);
        prev=(Button)findViewById(R.id.prev);
        play = (Button)findViewById(R.id.play);
        next=(Button)findViewById(R.id.next);
        showanswer=(Button)findViewById(R.id.icao_ShowAnswer);
        mSeekBar = (SeekBar)findViewById(R.id.seekbar);
        tv = (TextView)findViewById(R.id.tv);
        tv2 = (TextView)findViewById(R.id.tv2);
        mTimerText=(TextView)findViewById(R.id.icao_time);
        keywords=(TextView)findViewById(R.id.icao_keywords);
        mEditText=(EditText)findViewById(R.id.icao_edittext);
        mSeekBar.setOnSeekBarChangeListener(this);
        //使seekbar无法被用户手动调整
        mSeekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        play.setOnClickListener(this);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);
        showanswer.setOnClickListener(this);
        player = new MediaPlayer();
        Intent i=getIntent();
        typeid=i.getStringExtra("typeid");
        topicid=i.getStringExtra("topicid");
        new IcaoQuestionTask().execute();
        mCountDownTimer = new CountDownTimer(50000, 1000) {
            int animationCounter = 1;
            @Override
            public void onTick(long millisUntilFinished) {
                long remainingSeconds = millisUntilFinished / 1000;
                String timeText = String.format(Locale.getDefault(), "%02d:%02d", remainingSeconds / 60, remainingSeconds % 60);
                mTimerText.setText(timeText);
                if (remainingSeconds>=1&&remainingSeconds <= 10) {
                    animationCounter--;
                    if(remainingSeconds==10){
                        showanswer.setVisibility(View.INVISIBLE);
                        mTimerText.setTextColor(Color.RED);
                    }
                    if (animationCounter == 0) {
                        Animation fadeIn = new AlphaAnimation(0, 1);
                        fadeIn.setInterpolator(new DecelerateInterpolator());
                        fadeIn.setDuration(1000);
                        Animation fadeOut = new AlphaAnimation(1, 0);
                        fadeOut.setInterpolator(new AccelerateInterpolator());
                        fadeOut.setStartOffset(1000);
                        fadeOut.setDuration(1000);
                        AnimationSet animation = new AnimationSet(false);
                        animation.addAnimation(fadeIn);
                        animation.addAnimation(fadeOut);
                        mTimerText.startAnimation(animation);
                        animationCounter = 1;
                    }
                }
            }

            @Override
            public void onFinish() {
                mTimerText.setText("时间到了！");
//                mEditText.setText(getString(R.string.text_test));
                if(TimerComplete){
                    mEditText.setText(answerText);
                }


            }
        };
//        mCountDownTimer.start();
        if(checkNetworkAvailable(this)){
//            initMediaplayer();
        }else{
            Toast.makeText(this, "无网络！", Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * 初始化播放器
     */
    private void initMediaplayer() {
        try {
//            File file = new File(Environment.getExternalStorageDirectory()
//                    + "/Download/", "aiqiu.mp3");
//            player=MediaPlayer.create(getContext(), R.raw.q1);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            MyApplication app = (MyApplication)getApplication();
            System.out.println(app.getAddress()+"/upload/q1.m4a");
//            player.setDataSource(videoSource);
            Log.d("播放器", "file.toString()");
            player.setOnCompletionListener(this);
            player.setOnErrorListener(this);
//加载网络资源才使用
//            player.prepareAsync();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play:
                if (!player.isPlaying()) {
                    if(haDPlay){
                        haDPlay=false;
                        player.seekTo(0);
                    }
                    if(!TimerComplete){
                        TimerComplete=true;
                    }
                    player.start();
                    int totalTime = Math.round(player.getDuration() / 1000);
                    String str = String.format("%02d:%02d", totalTime / 60,
                            totalTime % 60);
                    tv2.setText(str);
                    mSeekBar.setMax(player.getDuration());
                    //保证runnable只添加一次，减少内存占用
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        if (mHandler.hasCallbacks(runnable)) {
                            // handler正在执行myRunnable
                            Log.d("Handler","true");
                        } else {
                            Log.d("Handler","false");
                            // handler没有执行myRunnable
                            mHandler.postDelayed(runnable, 1000);
                        }
                    }

                }

                break;
            case R.id.prev:
                if(videoNum==0){
                    Toast.makeText(IcaoActivity.this,"已经是第1题了",Toast.LENGTH_SHORT).show();
                    break;
                }else {
                    haDPlay=false;
                    TimerComplete=false;
                    mCountDownTimer.cancel();
                    mTimerText.setText("00:50");
                    mTimerText.setTextColor(Color.parseColor("#ffffff"));
                    mEditText.setText("");
                    showanswer.setVisibility(View.VISIBLE);
                    videoNum--;
                    MyApplication app = (MyApplication) getApplication();
                    String path = app.getAddress() + "/upload/" + questions[videoNum].getPath() + "/" + questions[videoNum].getName();
                    tv2.setText("总时间");
                    answerText=questions[videoNum].getAnswer();
                    keywords.setText(questions[videoNum].getKeywords());
                    try {
                        player.reset();
                        player.release();
                        player=new MediaPlayer();
                        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        player.setDataSource(path);
                        player.prepareAsync();
                        player.setOnCompletionListener(this);
                        player.setOnErrorListener(this);
                        haDTimer=false;
                        Toast.makeText(IcaoActivity.this,"当前是第"+(videoNum+1)+"题",Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
                break;
            case R.id.next:
                if(videoNum==(questions.length-1)){
                    Toast.makeText(IcaoActivity.this,"已经是最后一题了",Toast.LENGTH_SHORT).show();
                    break;
                }else{
                    TimerComplete=false;
                    haDPlay=false;
                    mCountDownTimer.cancel();
                    mTimerText.setText("00:50");
                    mTimerText.setTextColor(Color.parseColor("#ffffff"));
                    showanswer.setVisibility(View.VISIBLE);
                    mEditText.setText("");
                    videoNum++;

                    MyApplication app = (MyApplication) getApplication();
                    String path = app.getAddress() + "/upload/" + questions[videoNum].getPath() + "/" + questions[videoNum].getName();
                    tv2.setText("总时间");
                    answerText=questions[videoNum].getAnswer();
                    keywords.setText(questions[videoNum].getKeywords());
                    try {
                        player.reset();
                        player.release();
                        player=new MediaPlayer();
                        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        player.setDataSource(path);
                        player.prepareAsync();
                        player.setOnCompletionListener(this);
                        player.setOnErrorListener(this);
                        haDTimer=false;
                        Toast.makeText(IcaoActivity.this,"当前是第"+(videoNum+1)+"题",Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }

                }
                break;
            case R.id.icao_ShowAnswer:
                if(haDTimer){
                    showanswer.setVisibility(View.INVISIBLE);
                    mTimerText.setTextColor(Color.RED);
                    if (player.isPlaying()){
                        player.pause();
                    }

                    mCountDownTimer.onFinish();
                    mCountDownTimer.cancel();
//                    mEditText.setText(getString(R.string.text_test));
                    mEditText.setText(answerText);
                }
                break;
            default:
                break;
        }
    }
    public void initTimer(){

    }
    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (player != null&&!player.isPlaying()&&TimerComplete) {
            player.seekTo(seekBar.getProgress());
//            if(player.isPlaying()){
//                player.start();
//            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player != null) {
            player.stop();
            hadDestroy = true;
            player.release();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        Log.d("onCompletion","hadPlay");
        if(!haDTimer&&TimerComplete){
            mCountDownTimer.start();
            haDTimer=true;
        }
        haDPlay=true;
//        mHandler.removeCallbacks(runnable);
    }


    //判断网络连接是否可用（返回true表示网络可用，false为不可用）
    public boolean checkNetworkAvailable(Activity activity) {
        Context context = activity.getApplicationContext();
        //获取手机所有链接管理对象（包括对Wi-Fi，net等连接的管理）
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        } else {
            //获取NetworkInfo对象
            NetworkInfo[] info = manager.getAllNetworkInfo();
            if (info != null && info.length > 0) {
                for (int i = 0; i < info.length; i++) {
//                    System.out.println(i + "状态" + info[i].getState());
//                    System.out.println(i + "类型" + info[i].getTypeName());

                    // 判断当前网络状态是否为连接状态
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        Toast.makeText(this, "i="+i+"i1="+i1+",资源加载错误！", Toast.LENGTH_SHORT).show();
        return false;
    }

    private class IcaoQuestionTask extends AsyncTask<Void,Void, Question[]>{
        @Override
        protected Question[] doInBackground(Void... voids) {

            return new IcaoQuestionFetch().fetchItems(typeid,topicid);
        }
        @Override
        protected void onPostExecute(Question[] result) {
            if(result!=null){
                if(result.length==0){
                    Toast.makeText(IcaoActivity.this,"资源暂未导入",Toast.LENGTH_SHORT).show();
                    IcaoActivity.this.finish();
                    return;
                }
                questions=result;
                MyApplication app = (MyApplication)getApplication();
                String path=app.getAddress()+"/upload/"+result[0].getPath()+"/"+result[0].getName();
                answerText=result[0].getAnswer();
                keywords.setText(result[0].getKeywords());
                try {
                    player.setDataSource(path);
                    player.prepareAsync();
                    player.setOnCompletionListener(IcaoActivity.this);
                    player.setOnErrorListener(IcaoActivity.this);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }


                Toast.makeText(IcaoActivity.this,"选择成功，当前加载第一个题目",Toast.LENGTH_SHORT).show();
        }else{

            }
        }

    }


}